package com.example.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.data.repository.PartyRepository;

@RestController
public class PartyController {
	
	private final Logger log = LoggerFactory.getLogger(getClass());

	@Autowired
	private PartyRepository partyRepository;
	
	@GetMapping(value = "allParties")
	public ResponseEntity<Object> parties(Pageable pageable) {
		return ResponseEntity.ok()
				.body(this.partyRepository.findAll(pageable));
	}

}
