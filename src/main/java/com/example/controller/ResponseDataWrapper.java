package com.example.controller;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ResponseDataWrapper<T> {

	//Allows dynamic naming of the data collection (e.g. _embedded.parties)
	private Map<String, Collection<T>> embedded;

	@JsonProperty("_embedded")
	public Map<String, Collection<T>> getEmbedded() {
		return embedded;
	}

	public void setData(String propertyName, Collection<T> data) {
		this.embedded = new HashMap<>(1);
		this.embedded.put(propertyName, data);
	}
	
	public static <T> ResponseDataWrapper<T> wrap(String payloadName, Collection<T> data) {
		ResponseDataWrapper<T> wrapper = new ResponseDataWrapper<>();
		wrapper.setData(payloadName, data);
		return wrapper;
	}
}
