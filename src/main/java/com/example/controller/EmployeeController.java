package com.example.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.data.entity.Employee;
import com.example.data.entity.IdEntity;
import com.example.data.repository.EmployeeRepository;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * For brevity, this controller class contains code that would normally be separated out into different layers.
 */
@RestController
@RequestMapping("employees-custom")
public class EmployeeController {

	@Autowired
	private EmployeeRepository employeeRepository;
	
	@GetMapping("search")
	public ResponseEntity<Object> searchByFirstNameLastNameLike(@RequestParam(required = false) String firstName, @RequestParam(required = false) String lastName, Sort sort) 
	{
		List<Employee> results = StringUtils.isEmpty(firstName) && StringUtils.isEmpty(lastName) ?
				this.employeeRepository.findAll(sort) : this.employeeRepository.findTest(firstName, lastName, sort);
				
		ResponseDataWrapper<Employee> wrapper = ResponseDataWrapper.wrap("employees", results);
		return ResponseEntity.ok().body(wrapper);
	}
	
	@PatchMapping("{id}")
	public ResponseEntity<Object> updateCascading(@PathVariable("id") Employee employee, @RequestBody UpdatedEmployee updatedEmployee, BindingResult bindingResult)
	{
		if(bindingResult.hasErrors()) {
			return ResponseEntity.ok()
					.body(bindingResult);
		}
		
		if(updatedEmployee.getFirstName() != null) employee.getParty().setFirstName(updatedEmployee.getFirstName());
		if(updatedEmployee.getLastName() != null) employee.getParty().setLastName(updatedEmployee.getLastName());
		if(updatedEmployee.getTerminationDate() != null) employee.setTerminationDate(updatedEmployee.getTerminationDate());
		
		employee = this.employeeRepository.save(employee);

		return ResponseEntity.ok().body(employee);
	}
	
	public static class UpdatedEmployee 
	{
		private String firstName;
		
		private String lastName;
		
		@JsonFormat(pattern = IdEntity.DATE_FORMAT)
		private Date terminationDate;
		
		public String getFirstName() {
			return firstName;
		}
		
		public void setFirstName(String firstName) {
			this.firstName = firstName;
		}
		
		public String getLastName() {
			return lastName;
		}
		
		public void setLastName(String lastName) {
			this.lastName = lastName;
		}
		
		public Date getTerminationDate() {
			return terminationDate;
		}

		public void setTerminationDate(Date terminationDate) {
			this.terminationDate = terminationDate;
		}
	}
}
