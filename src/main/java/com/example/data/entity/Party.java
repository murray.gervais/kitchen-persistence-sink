package com.example.data.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "party")
public class Party extends IdEntity {

	@Column(name = "first_name", nullable = false, length = 32)
	private String firstName;
	
	@Column(name = "last_name", nullable = false, length = 32)
	private String lastName;

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
}
