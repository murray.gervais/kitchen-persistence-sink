package com.example.data.entity;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue(FullTimeEmployee.DISCRIMINATOR)
public class FullTimeEmployee extends Employee {
	
	public static final String DISCRIMINATOR = "FT";
	
	private Double salary;

	public Double getSalary() {
		return salary;
	}

	public void setSalary(Double salary) {
		this.salary = salary;
	}
}
