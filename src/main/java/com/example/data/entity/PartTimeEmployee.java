package com.example.data.entity;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue(PartTimeEmployee.DISCRIMINATOR)
public class PartTimeEmployee extends Employee {

	public static final String DISCRIMINATOR = "PT";
	
	private Integer rate;

	public Integer getRate() {
		return rate;
	}

	public void setRate(Integer rate) {
		this.rate = rate;
	}
	
}
