package com.example.data.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.data.entity.FullTimeEmployee;

@Repository
public interface FullTimeEmployeeRepository extends JpaRepository<FullTimeEmployee, Long> {

}
