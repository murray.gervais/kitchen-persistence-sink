package com.example.data.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.data.entity.PartTimeEmployee;

@Repository
public interface PartTimeEmployeeRepository extends JpaRepository<PartTimeEmployee, Long> {

}
