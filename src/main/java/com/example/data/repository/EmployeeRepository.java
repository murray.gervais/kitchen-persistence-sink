package com.example.data.repository;

import java.util.List;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.data.entity.Employee;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Long> {

	public static final String QUERY_FIRST_NAME_OR_LAST_NAME_LIKE = "SELECT e FROM Employee e WHERE "
			+ "(:firstName IS NULL OR LOWER(party.firstName) LIKE CONCAT(LOWER(:firstName), '%')) "
			+ "AND (:lastName IS NULL OR LOWER(party.lastName) LIKE CONCAT(LOWER(:lastName), '%'))";
		
	@Query(value = QUERY_FIRST_NAME_OR_LAST_NAME_LIKE)
	public List<Employee> findTest(@Param("firstName") String firstName, @Param("lastName") String lastName, Sort sort);
}
