package com.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KitchenPersistenceSinkApplication {

	public static void main(String[] args) {
		SpringApplication.run(KitchenPersistenceSinkApplication.class, args);
	}
}
