--
-- Simple Kitchen Persistence Sink Example data
--

insert into department(name) values ('IT');
SET @deptIdIT = identity();

insert into department(name) values ('HR');
SET @deptIdHR = identity();

insert into party(first_name, last_name) values ('Joe', 'Smith');
insert into employee(party_id, department_id, employee_type, commencement_date, termination_date, salary) values (identity(), @deptIdIT, 'FT', '2004-01-15', '2006-04-24', 95000);

insert into party(first_name, last_name) values ('Scott', 'Wilson');
insert into employee(party_id, department_id, employee_type, commencement_date, termination_date, salary) values (identity(), @deptIdHR, 'FT', '2001-10-23', null, 80000);

insert into party(first_name, last_name) values ('Mike', 'Canmore');
insert into employee(party_id, department_id, employee_type, commencement_date, termination_date, rate) values (identity(), @deptIdIT, 'PT', '2008-02-05', null, 50.45);




--
-- Following tables are used for inheritance examples
--

