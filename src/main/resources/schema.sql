create table employee (
	id bigint auto_increment not null,
	party_id bigint not null,
	employee_type char(2) not null,
	department_id bigint not null,
	commencement_date date not null,
	rate int,
	salary double,
	termination_date date,
	constraint pk_employee_id primary key (id),
	constraint uk_employee_party_id unique (party_id),
);

create table department (
	id bigint auto_increment not null,
	name varchar(32) not null
);

create table party (
	id bigint auto_increment not null,
	first_name varchar(32) not null,
	last_name varchar(32) not null,
	constraint pk_party_id primary key (id)
);

alter table employee add constraint fk_employee_party_id foreign key (party_id) references party(id);
alter table employee add constraint fk_employee_dept_id foreign key (department_id) references department(id);
