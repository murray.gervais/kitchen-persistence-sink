# Persistence Kitchen Sink

This application demonstrates several custom strategies within the Spring Data JPA realm.  The application also includes Spring Data REST, as well as the following
features:

* H2 in-memory database
* H2 console

## Quick Start

Prerequisites:

* Java 8 JDK
* Gradle 3.X

### Run on Command Line

To start the application from the command line simply navigate into the project root directory and execute the following command:

    gradle bootRun
    
### Import to Eclipse

MyEclipse or Spring Tool Suite are the best IDEs to run the application in, since they have built in tooling for Spring Boot.  That said, basic Eclipse is fine to.
Simply run the following command from the command line while in the project root directory:

    gradle cleanEclipse eclipse
    
Then use the Eclipse Import Existing Project wizard to import the project.  You can then run the application as a Spring Boot App within MyEclipse or Spring Tool Suite.  If using
basic Eclipse, it can be run as a Java Application, using the *KitchenPersistenceSinkApplication* class as the main application class.

## Data Model

This application uses an extremely simple data model, based around an *employee*, *party* and *department* tables.  Where the *employee* table contains employee-specific
data, the *party* table contains normalized contact information.

* Party - Contains normalized contact information (first name and last name)
* Department - Organization department information
* Employee - Contains employee-specific information, and has foreign key relationships to both party and department tables.  The employee table implements a single-table
inheritance strategy, using a discriminator column to differentiate between full-time and part-time employees.  The full-time employee entity has a *salary* field/column, 
whereas the part-time employee entity has a *rate* field/column. 

## HAL Browser

The HAL Browser (for Spring Data REST) is included in this application, and allows navigation of the Spring Data REST API through a web interface.  Note, that this interface
does **not** cover the custom controllers/endpoints that are discussed in this document, but it can be used to inspect the same underlying entities.  The HAL Browser is available
at the application context root:

    http://localhost:8080 

## Inheritance

As mentioned in the data model description, the *employee* table implements a single-table inheritance scheme, using the *employee_type* column as a discriminator.  Two
entities inherit from the generic *employee* entity: part-time and full-time entities.

A Spring Data REST repository is exposed for each of the three entity types (including the generic *employee*).  The HAL browser can be used to query these repositories and
verify that the appropriate entities are returned for each.  You can note that for the generic *employee* repository, both full-time and part-time employee entities are returned and
that each serialized (as JSON) entity contains only the fields specific to it's entity type.  


## Custom Controller/Endpoint Examples

The following examples are implemented using simple custom Spring controllers.  Links to these endpoints will not appear in the HAL Browser since they are not part
of Spring Data REST.

### Employee Search (with related party entity fields)

GET Request URL

    http://localhost:8080/employees-custom/search?firstName=M&lastName=C&sort=party.lastName,asc

Demonstrates:

* Related entity (*party*) fields are included inline
* Search by related entity field (*party.firstName*)
* Sort by related entity field (*party.lastName*)
* All parameters are optional

### Employee Cascading Update

PATCH Request URL

    http://localhost:8080/employees-custom/2
    
Example Request Body

    {
		"lastName": "Orbison",
		"firstName": "Roy",
		"terminationDate": "2016-01-05"
    }

Demonstrates:

* The employee identified by the path variable (2 in the example above) is partialy updated, along with two of the related employee fields
* In this example, patch accepts *lastName*, *firstName* and *terminationDate* fields


## H2 Console

The in-memory database schema is created and populated on startup from the following files, located in the sr/main/resources project directory:

* schema.sql
* data.sql

When the application is running, you can access the H2 web console by visiting the following url:

    http://localhost:8080/h2-console

Use the following JDBC URL to access the database:

    jdbc:h2:mem:pks